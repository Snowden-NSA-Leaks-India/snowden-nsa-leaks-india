# Snowden NSA Leaks — India
## NSA's India related activities

This report documents all the intelligence operations conducted by NSA against India, 
or NSA activities relevant to Indian national security, revealed in the Snowden leaks.

Emphasis mine.

Thanks a lot to /u/gryphus-scarface for helping with this research.

---

## Index

  - [US uses its embassies as covert SIGINT collection and surveillance sites, 2010](#us-uses-its-embassies-as-covert-sigint-collection-and-surveillance-sites)
  - [US exploits China's compromise of Indian Embassy computers in Beijing, 2006](#us-exploits-chinas-compromise-of-indian-embassy-computers-in-beijing)
  - [US lists 9 Indian "targets" in the Indian UN Mission and Indian Embassy in US for surveillance, 2010](#us-lists-9-indian-targets-in-the-indian-un-mission-and-indian-embassy-in-us-for-surveillance)
  - [US satellite intercept station located in New Delhi, undated](#us-satellite-intercept-station-located-in-new-delhi)
  - [US SIGINT's key strategic goals related to India, 2007](#us-sigints-key-strategic-goals-related-to-india)
  - [US geolocated India's nuclear weapon storage facility, 2005](#us-geolocated-indias-nuclear-weapon-storage-facility)
  - [UK/US compromised SIM cards worldwide, including India, 2010](#ukus-compromised-sim-cards-worldwide-including-india)
  - [US used third-party hackers to spy on Indian diplomatic missions and Indian Navy, 2010](#us-used-third-party-hackers-to-spy-on-indian-diplomatic-missions-and-indian-navy)
  - [US successfully breaks into Pakistani General Intelligence VPN (also breaking other SSH, SSL, etc), 2010](#us-successfully-breaks-into-pakistani-general-intelligence-vpn-also-breaking-other-ssh-ssl-etc)
  - [US officials share contact info of foreign civilian and military leaders with NSA for tapping, 2006](#us-officials-share-contact-info-of-foreign-civilian-and-military-leaders-with-nsa-for-tapping)
  - [US established temporary SCS site in Hyderabad for US Presidential visit, 2006](#us-established-temporary-scs-site-in-hyderabad-for-us-presidential-visit)
  - [US partnered with Germany for intel collection against Indian satellite telemetry, 2006](#us-partnered-with-germany-for-intel-collection-against-indian-satellite-telemetry)
  - [US aims for "sustained access to Indian leadership communications", 2006](#us-aims-for-sustained-access-to-indian-leadership-communications)
  - [NZ provides intel to US on Indian diplomatic communications, 2013](#nz-provides-intel-to-us-on-indian-diplomatic-communications)
  - [US probably launching recon aircraft from India, Thailand, Pak, 2001](#us-probably-launching-recon-aircraft-from-india-thailand-pak)
  - [US recon aircraft targeting Indian communications, 2001](#us-recon-aircraft-targeting-indian-communications)
  - [US recon aircraft crash in China made India vulnerable to cyber warfare, 2001](#us-recon-aircraft-crash-in-china-made-india-vulnerable-to-cyber-warfare)
  - [Intelligence relationship between India and US, 2009](#intelligence-relationship-between-india-and-us)

<br />

  - [Sources](#sources)

---

### US uses its embassies as covert SIGINT collection and surveillance sites

#### 2010-??-??

US Embassies worldwide have covert SIGINT collection sites installed on them, as part of the NSA's STATEROOM surveillance program, run 
by the Special Collection Service (SCS) unit, whose teams are officially sent as "diplomats".

Here, the unit operates devices to intercept cellular signals, wireless networks, and satellite communications in a wide area around the embassy. 
The purpose of these interceptions is access to national leadership communications.

Note that key Indian government headquarters like the Parliament, the Ministry of Defence, Reserve Bank, 
Secretariat, Ministry of Home Affairs, DRDO Bhavan, etc. are located within 2 to 3 km of the US Embassy in New Delhi, 
and definitely do fall into its range of surveillance (see below).

This capability is also evidenced in [*US exploits China's compromise of Indian Embassy computers in Beijing, 2006*](#us-exploits-china-s-compromise-of-indian-embassy-computers-in-beijing). Here the US Embassy in Beijing uses its SCS facility to 
surveil Indian and Singapore Embassies. Note that the Singapore Embassy is 4.5 km away from the US Embassy, so its range is at least that, 
thus enough to cover all major Indian central government offices from the US embassy in New Delhi.

This capability is complemented by NSA's access to decryption keys for 3G/4G/LTE traffic of millions of SIM cards, see [*UK/US compromised SIM cards worldwide, including India, 2010*](#ukus-compromised-sim-cards-worldwide-including-india).

See also [*US satellite intercept station located in New Delhi, undated*](#us-satellite-intercept-station-located-in-new-delhi), which is also 
run by the SCS unit.

**Sources:**
  - [80 SCS Sites Map, released by *Der Spiegel*](sources/der-spiegel/vpcs-deploy-80-scs-sites.png)
  - [Driver 1: Worldwide SIGINT/Defense Cryptological Platform Map, released by *NRC Handelsblad*](sources/nrc-handelsblad/nsa568-nsa-driver1-worldwide-sigint.jpg)
  - [Close Access SIGADS document, released by *L'Espresso Repubblica*](sources/espresso-repubblica/scs-collection-types.jpg)
  - [Uncensored Close Access SIGADS document](sources/edwardsnowden-com/closeaccess1.pdf)
  - [SCS Sites document, released by *L'Espresso Repubblica*](sources/espresso-repubblica/scs-sites.jpg)
  - [STATEROOM Guide](sources/snowden-archive/20131027-derspiegel__embassy.pdf)
  - [Article in *Der Spiegel*:  The NSA's Secret Spy Hub in Berlin](sources/der-spiegel/the-nsas-secret-spy-hub-in-berlin.pdf)
  - [The State of Covert Collection -- An Interview with SCS Leaders (part 1)](sources/sidtoday/2006-11-15_SIDToday_-_The_State_of_Covert_Collection_--_An_Interview_with_SCS_Leaders_part_1.pdf)
  - [Article by investigative journalist Duncan Campbell: The Embassy Spy Centre Network](sources/duncan-campbell/embassy-spy-centre-network.pdf)

(SIGADS = SIGINT Activity Designators)

![80 SCS Sites Map, released by *Der Spiegel*](sources/der-spiegel/vpcs-deploy-80-scs-sites.png)

![SCS Sites document, released by *L'Espresso Repubblica*](sources/espresso-repubblica/scs-sites.jpg)

> (TS//COMINT//X1) Special Collection Service (SCS). **Covert SIGINT collection abroad from official U.S. Government establishments, 
typically U.S. embassies and consulates.** The NSA partners with the CIA in the SCS construct in which NSA employees under 
diplomatic cover conduct SIGINT collection. Special Collection Sites provide considerable perishable **intelligence on leadership 
communications** largely facilitated by site presence within a national capital.

<details><summary><i>Click to expand:</i> Description of the US Embassy SIGINT collection site structures in [Article in *Der Spiegel*:  The NSA's Secret Spy Hub in Berlin](sources/der-spiegel/the-nsas-secret-spy-hub-in-berlin.pdf)</summary>

> The necessary equipment is usually installed on the upper floors of the embassy
buildings or on rooftops where the technology is covered with screens or Potemkin-
like structures that protect it from prying eyes.
>
> That is apparently the case in Berlin, as well. SPIEGEL asked British investigative
journalist Duncan Campbell to appraise the setup at the embassy. In 1976,
Campbell uncovered the existence of the British intelligence service GCHQ. In his
so-called "Echelon Report" in 1999, he described for the European Parliament the
existence of the global surveillance network of the same name.
>
> Campbell refers to window-like indentations on the roof of the US Embassy. They
are not glazed but rather veneered with "dielectric" material and are painted to
blend into the surrounding masonry. This material is permeable even by weak radio
signals. The interception technology is located behind these radio-transparent
screens, says Campbell. The offices of SCS agents would most likely be located in
the same windowless attic.
>
> This would correspond to internal NSA documents seen by SPIEGEL. They show, for
example, an SCS office in another US embassy -- a small windowless room full of
cables with a work station of "signal processing racks" containing dozens of plug-in
units for "signal analysis."
>
> The SCS are careful to hide their technology, especially the large antennas on the
roofs of embassies and consulates. If the equipment is discovered, explains a "top
secret" set of classified internal guidelines, it "would cause serious harm to
relations between the United States and a foreign government."

</details>

<br/>

See [Article by investigative journalist Duncan Campbell: The Embassy Spy Centre Network](sources/duncan-campbell/embassy-spy-centre-network.pdf) for 
satellite images of probable SIGINT collection sites on US embassies around the world.

---

### ~~US attacks and attempts to sabotage Indian and Russian superconductivity research~~

~~US finds that India and Russia are working together on superconductivity research. US then makes separate approaches to India 
and Russia to break up the partnership, and conducts cyber operations against the research facilities involved in this.~~

**This section has been removed because it was based on a misunderstanding of the source text.**

**Sources:**
  - [Quadrennial Intelligence Community Review](sources/snowden-archive/20140905-theintercept__quadrennial_intelligence_community_review_final_report.pdf)


---

### US exploits China's compromise of Indian Embassy computers in Beijing

#### 2006-08-28

An NSA Special Collection Service (SCS) team finds a way to spy on the Indian Embassy, Beijing. In the process, the team 
discovers China also spying on the Indian Embassy, and exploits that for easy access.

Apparently, these were done by compromising internal embassy computers, since all outbound communication was securely encrypted.

See also [*US uses its embassies as covert SIGINT collection and surveillance sites, 2010*](#us-uses-its-embassies-as-covert-sigint-collection-and-surveillance-sites).

**Sources:**
  - [The Wizards of OZ II: Looking Over the Shoulder of a Chinese C2C Operation](sources/sidtoday/2006-08-28_SIDToday_-_The_Wizards_of_OZ_II_Looking_Over_the_Shoulder_of_a_Chinese_C2C_Operation.pdf)

> (TS//SI//REL) Once upon a time in China... a joint MUSKETEER/Special Collection Service (SCS) team deployed to Beijing to survey 802.11 wireless LAN targets for sustained collection. The MUSKETEER OZ II survey revealed several high-interest targets accessible from US-968U (Beijing), **including the embassies of India, Singapore, Pakistan, Colombia, and Mongolia**.
>
> (TS//SI//REL) While analyzing the Indian Embassy's diplomatic communications, **the team discovered that possibly Chinese Government-sponsored entities have compromised several of the computers inside the Indian Embassy**. The analysis of outgoing communications showed that someone was exfiltrating approximately ten sensitive diplomatic documents per day through covert channels to drop box hosts located on the public Internet. Exfiltrated files were often Microsoft Office-compatible files or Adobe PDF documents.
>
> (TS//SI//REL) The team identified the procedure by which the files are being exfiltrated and tasked them for sustained collection. This action **provided access to sensitive Indian diplomatic files that otherwise would not have been collected because of the high level of encryption employed on the Indians' outgoing communications.**

(US-968U is the codename for the US Embassy in Beijing)

---

### US lists 9 Indian "targets" in the Indian UN Mission and Indian Embassy in US for surveillance

#### 2010-09-10

Close Access SIGADS lists 9 Indian "targets" in the US for SCS collection:
  - 4 targets in Indian UN Mission in New York
  - 5 targets in Indian Embassy in Washington

Collection types were:
  - HIGHLANDS (implants)
  - MAGNETIC (magnetic emanation sensing)
  - VAGRANT (computer screen collection)
  -LIFESAVER (hard drive imaging)

**Sources:**
  - [Uncensored Close Access SIGADS document](sources/edwardsnowden-com/closeaccess1.pdf)

---

### US satellite intercept station located in New Delhi

#### ????-??-??

Among several other locations, the US has a satellite intercept ("FORNSAT") station in New Delhi run by the SCS unit.

The association with SCS hints that this station may be located in the US Embassy in New Delhi, see [*US uses its embassies as covert SIGINT collection and surveillance sites, 2010*](#us-uses-its-embassies-as-covert-sigint-collection-and-surveillance-sites).

A similar FORNSAT station in Thailand, called "LEMONWOOD", was key to the US gaining intelligence of the location of Indian nuclear 
weapons storage facilities, see [*US geolocated India's nuclear weapon storage facility, 2005*](#us-geolocated-indias-nuclear-weapon-storage-facility).

**Sources:**
  - [Primary FORNSAT Collection Operations, released by *Der Spiegel*](sources/der-spiegel/primary-fornsat-collection-ops.pdf)

---

### US SIGINT's key strategic goals related to India

NSA details its level of SIGINT focus on various aspects of India: government, foreign policy, military, 
science and technology, weapons, intelligence, etc.

#### 2007-01-??

**Sources:**
  - [United States SIGINT System January 2007 Strategic Mission List](sources/snowden-archive/20131104-thenewyorktimes__sigint_goals.pdf)

> United States SIGINT System
>
> January 2007 Strategic Mission List

> 1) **Focus Areas - critically important targets against which the SIGINT enterprise is placing emphasis.**
DIRNSA designation of a target as a focus area constitutes his guidance to the SIGINT System that it is a
"must do” target for that mission
>
> 2) **Accepted Risks — strategically significant targets against which the USSS is not placing emphasis** and
for which SIGINT should not be relied upon as a primary source. DIRNSA's reasons for accepting these
risks include high difficulty and lack of resources or as an “Economy of Force Measure,” in order to
achieve focus on the most critical targets.

(USSS = US SIGINT System)

> **C. MISSION: WMP and CBRN Programs and Proliferation :
Combating the Threat of Development and Proliferation of Weapons of Mass Destruction, CBRN,
and Delivery Methods (particularly ballistic and nuclear-capable cruise missiles).**
>
> Focus Areas:
>
> b. State WMD and ballistic and cruise missile programs of:
>
>   **India (nuclear, ballistic missiles)**
>
>   **Pakistan (nuclear, ballistic missiles)**
>
> c. WMD and missile proliferation activities by states:
>
>   **Pakistan (nuclear, ballistic missiles)**
>
> d. WMD and missile acquisition activities by states:
>
>   **India (cruise missiles)**
>
>   **Pakistan (cruise missiles)**
>
> e. Safety and Security of WMD:
>
>   **Pakistan (nuclear)**


> **E. MISSION: State/Political Stability: Providing Warning of Impending State Instability.**
>
> Focus Areas:
> 
> a. Internal political activities that could threaten the survivability of leadership in **countries where US
has interest in regime continuity:** Iraq, **Afghanistan, Pakistan**, and Saudi Arabia.
>
> Accepted Risks:
> 
>   Internal political stability of: Egypt, Zimbabwe, Cote d'Ivoire, Liberia, DROC,
**Bangladesh**, Georgia, Jordan, and Haiti.


> **G. MISSION: Regional Conflict and Crisis/Flashpoints to War: Monitoring Regional 
Tensions that Could Escalate to Conflict/Crisis.**
>
> Focus Areas:
>
>   **Regional flashpoints** that could pose a significant threat to U.S. strategic interests:
Arab, Iran-Israel conflict, Korean Peninsula, China-Taiwan, **India-Pakistan**, Venezuela (impact on
surrounding region), and Russia/Georgia.


> **H. MISSION: Information Operations: Mastering Cyberspace and 
Preventing an Attack on U.S. Critical Information Systems.**
>
> Accepted Risks:
>
>   c. **FIS Cyber Threat:** France, Israel, Cuba, **India**, and North Korea
>
>   e. **Enabling Influence Operations:** **Pakistan** and Russia

(FIS Cyber Threat = "Foreign Intelligence Services' Cyber Threat Activities: Deliver intelligence on the capabilities.
vulnerabilities, plans and intentions of foreign actors to conduct CNO against USG networks and
those of interest to the USG. Identify what Foreign Intel Services know about USG capabilities,
vulnerabilities, plans and intentions to conduct CNO" [direct quote from source]

Enabling Influence Operations = "Support U.S. military deception (MILDEC) and psychological
operations (PSYOP), and inter-agency Strategic Communication objectives to influence target
behavior and activities" [direct quote from source]

CNO = Computer Network Operations, USG = US Govt)

> **I. MISSION: Military Modernization: Providing Early Detection of Critical Developments in
Foreign Military Programs.**
>
> Focus Areas:
> 
>   a. Threat posed by continued modernization as it involves the forces and weapons of: China, North
Korea, Russia, Iran, and Syria.
>
>   b. Activities of state and non-state actors (gray arms dealers) in supplying advanced conventional
weapons.
>
>   c. Threats posed by foreign space and counter-space systems: China and Russia
> 
> Accepted Risks:
>
>   a. **Weapons and force developments in:** Saudi Arabia, and **India**
>
>   b. **Threats posed by foreign space and counter-space systems: India** and France


> **J. MISSION: Emerging Strategic Technologies: Preventing Technological Surprise.**
>
> Focus Areas:
>
>   **Critical technologies that could provide a strategic military, economic, or political
advantage:** high energy lasers, low energy lasers, advances in computing and information technology,
directed energy weapons, stealth and counter-stealth, electronic warfare technologies, space and remote
sensing, electro-optics, nanotechnologies, energetic materials. **The emerging strategic technology threat is
expected to come mainly from** Russia, China, **India**, Japan. Germany, France, Korea, Israel, Singapore, and
Sweden.
>
> Accepted Risks:
>
>   Technological advances and/or basic S&T development on a global basis elsewhere.


> **K. MISSION: Foreign Policy ((includes Intention of Nations and Multinational Orgs)): Ensuring Diplomatic Advantage for the US.**
>
> Focus Areas:
>
>   **Positions, objectives, programs, and actions on the part of governments or multilateral
organizations that could significantly impact U.S. national security interests:** China, Russia, France,
Germany, Japan. Iran. Israel. Saudi Arabia, North Korea, Afghanistan. Iraq, UN, Venezuela, Syria.
Turkey, Mexico, South Korea, **India, and Pakistan**.


> **M. MISSION: Foreign Intelligence, Counterintelligence; Denial & Deception Activities:
Countering Foreign Intelligence Threats.**
>
> Focus Areas:
>
>   Espionage/intelligence collection operations and manipulation/influence **operations
conducted by foreign intelligence services** directed against U.S. government, military, science &
technology and Intelligence Community from: China, Russia, Cuba, Israel, Iran, **Pakistan**, North Korea.
France, Venezuela, and South Korea
>
> Accepted Risks:
>
>   Espionage/intelligence collection operations against U.S. government, military, science
& technology and Intelligence Community from: Taiwan and Saudi Arabia


> **O. MISSION: Economic Stability/Influence: Ensuring U.S. Economic Advantage and Policy
Strategies.**
>
> Focus Areas:
>
>   Economic stability, financial vulnerability, and economic influence of states of strategic
interest to the US: China, Japan, Iraq, and Brazil.
>
> Accepted Risks:
>
>   **Economic stability, financial vulnerability, and economic influence of states of strategic
interest to the US:** Turkey and **India**.

---

### UK/US compromised SIM cards worldwide, including India

#### 2010-??-??

GCHQ (UK) hacked into Gemalto, the world's largest SIM card manufacturer, and stole encryption keys which would have allowed them 
to easily decrypt 3G/4G/LTE cell phone traffic. Operation codename "Dapino Gamma".

Further reading: https://theintercept.com/2015/02/19/great-sim-heist/


**Sources:**
  - [PCS Harvesting at Scale](sources/snowden-archive/20150219-theintercept__pcs_harvesting_at_scale.pdf)
  - [IMSIs Identified with Ki data for Network Providers](sources/snowden-archive/20150219-theintercept__imsis_identified_with_ki_data_for_network.pdf)
  - [Dapino Gamma Target Personalisation centres (non 5 eyes)](sources/snowden-archive/20150219-theintercept__dapino_gamma_target_personalisation_centres.pdf)


> **PCS Harvesting at Scale**
>
> Individual Subscriber Authentication Keys, or Ki values, are required to **decrypt GSM
communications**. They are stored both on the mobile user's SIM card and at a Home Location
Register operated by the provider. TDSD has developed a methodology for **intercepting these
keys as they are transferred between various network operators and SIM card providers**. This
is now a core part of TDSD's business carried out by analysts in the team.

(TDSD = Target Discovery and SIGINT Development)

> IMSI results broken down by network code
>
> **IDEACL, INDIA**
>
> **IDEAMB, INDIA**
>
> **VDFNDG, INDIA**

(probably Idea Cellular, Idea Mobile, Vodafone)

> IMSIs Identified with Ki data for Network Providers
>
> **IDEACL, INDIA**

(probably Idea Cellular)

> Target Personalisation centres (non 5 eyes)
>
> **Noida India**

(in this context, "personalisation centres" are facilities which convert a generic SIM into a provider-specific one)

---

### US used third-party hackers to spy on Indian diplomatic missions and Indian Navy

#### 2010-05-06

NSA found third-party independent or state-sponsored hackers harvesting emails of NSA's targets, including Indian targets.
NSA used this info for its own gain, and made no attempts to inform India.

Note that the complete list of third-party hacker targets seems to indicate Chinese hackers, since most of the list are groups which China 
perceives as an enemy.

**Sources:**
  - [Who Else Is Targeting Your Target? Collecting Data Stolen by Hackers](sources/snowden-archive/20150204-theintercept__who_else_is_targeting_your_target_collecting.pdf)

> (TS//SI//REL) **Hackers are stealing the emails of some of our targets... by collecting the hackers' "take," we 1) get access to the emails themselves and 2) get insights into who's being hacked.**
>
> (U//FOUO) Victim Set
>
> (TS//SI//REL) INTOLERANT traffic is very organized. Each event is labeled to identify and categorize victims. Cyber attacks commonly apply descriptors to each victim - it helps herd victims and track which attacks succeed and which fail. **Victim categories** make INTOLERANT interesting:
>
> **A = Indian Diplomatic & Indian Navy**
>
>B = Central Asian diplomatic
>
>C = Chinese Human Rights Defenders
>
>D = Tibetan Pro-Democracy Personalities
>
>E = Uighur Activists
>
>F = European Special Rep to Afghanistan and Indian photo-journalism
>
>G = Tibetan Government in Exile
>
> (TS//SI//REL) **New victims appear to flood out their entire inbox, going back months or, even, years. Then only new mail is transmitted. Hundreds of emails are seen on an average day.**

---

### US geolocated India's nuclear weapon storage facility

#### 2005-01-13

NSA SIGINT geolocates an Indian nuclear weapon storage facility, and identifies types of weapons stored.

These signals were isolated using a FORNSAT collection facility in Thailand, called "LEMONWOOD". 
A similar station is located in New Delhi itself, see [*US satellite intercept station located in New Delhi, undated*](#us-satellite-intercept-station-located-in-new-delhi).

**Sources:**
  - [New Collection Access Yields Spectacular Intel](sources/sidtoday/2005-01-13_SIDToday_-_New_Collection_Access_Yields_Spectacular_Intel.pdf)

> (TS//SI) In October 2004, **RAINFALL successfully geolocated signals of a suspected Indian
nuclear weapons storage facility.** This prompted a Foreign Satellite (FORNSAT) collection facility,
LEMONWOOD, and the Unidentified Signal and Protocol Analysis Branch (S31124) at NSA to
collaborate in isolating these signals and, through signals development, confirm their content as
related to Indian nuclear weapons. This breakthrough highlighted the need to deploy additional
demodulating equipment to LEMONWOOD in order to expand a modest collection effort
undertaken since the signal was discovered in October.
>
> (TS//SI) Immediately after fielding this equipment, collection of this new network
began to provide what is being called "spectacular" activity. **Exploitation of that collection revealed
India's first-ever SAGARIKA Submarine-Launched Ballistic Missile (SLBM) launch; 
DHANUSH sea-launched Short Range Ballistic Missile (SRBM); and pilotless target aircraft.**
>
> (TS//SI) Collection from this new access has also provided **significant intelligence on India's
possession of two different types of airdropped bombs, one believed to be a very large Fuel Air
Explosive (FAE) bomb** of an unidentified type. The other, not yet confirmed by the analytic
community, **may be a new generation of airdropped nuclear weapons.**

---

### US successfully breaks into Pakistani General Intelligence VPN (also breaking other SSH, SSL, etc)

NSA presentation details process of breaking VPNs, SSL, SSH, PPTP, IPSec, etc, along with successes.

#### 2010-09-03

**Sources:**
  - [Intro to the VPN Exploitation Process](sources/snowden-archive/20141228-derspiegel__intro_to_the_vpn_exploitation_process_mentioning_the_protocols_attacked_pptp_ipsec_ssl_ssh.pdf)

> Success 2: PPTP
>
> Government
>
> Mexican Diplomatic, MXDBB
>
> **Pakistani General Intelligence, PKRAQ**
>
> Turkish Diplomatic, TUDAT
>
> Afghanistan Government, AFYAD

---

### US officials share contact info of foreign civilian and military leaders with NSA for tapping

#### 2006-10-27

**Sources:**

> (C) Customers Can Help SID Obtain Targetable Phone Numbers
>
> (C) From time to time, SID is offered access to the personal contact databases of US officials. Such "rolo-
dexes" may contain contact information for foreign political or military leaders, to include direct line, fax, resi­dence and cellular numbers.
>
> (C) In one recent case, a US official provided NSA with 200 phone numbers to 35 world leaders. S2 Opera­tions 
Staff immediately supplied this information to the S2 Production Centers (PCs). Despite the fact that
the majority is probably available via open source, the PCs have noted 43 previously unknown phone num­bers. 
These numbers plus several others have been tasked to OCTAVE.
>
> (C) Thus far, the PCs have noted little reportable intelligence from these particular numbers, which appear
not to be used for sensitive discussions. However, these numbers have provided lead information to other
numbers that have subsequently been tasked.
>
> (C) This success leads S2 to wonder if there are NSA liaisons whose supported customers may be willing to
share their rolodexes or phone lists with NSA as potential sources of intelligence. S2 welcomes such infor­mation! 
Anyone who can help us obtain such phone lists should contact the S203 Operations Staff Access Team

(SID = Signals Intelligence Directorate

OCTAVE is the NSA's system for telephone targeting.

In NSA parlance, "customer" refers to any other government agency which uses NSA intelligence.)

---

### US established temporary SCS site in Hyderabad for US Presidential visit

#### 2006-03-02

NSA's SCS team establishesa temporary site in Hyderabad for surveillance during visit of George W. Bush. Probably the US Consulate in Hyderabad.

See [*US uses its embassies as covert SIGINT collection and surveillance sites, 2010*](#us-uses-its-embassies-as-covert-sigint-collection-and-surveillance-sites)

**Sources:**
  - [Temporary SCS Site Established for Presidential Visit to India; Threats to President Reported](sources/sidtoday/2006-03-02_SIDToday_-_Temporary_SCS_Site_Established_for_Presidential_Visit_to_India_Threats_to_President_Reported.pdf)

---

### US partnered with Germany for intel collection against Indian satellite telemetry

#### 2006-03-02

**Sources:**
  - [The End of Germany's Prime Satellite-Telemetry Collection Site](sources/sidtoday/2006-03-02_SIDToday_-_The_End_of_Germanys_Prime_Satellite-Telemetry_Collection_Site.pdf)

> (S//SI) **Since 1976, Germany's Bundesnachrichtendienst** (BND -- the Federal Intelligence
Service) has been a key partner in NSA's worldwide satellite telemetry collection and analysis
efforts. NSA has relied on the German PACKHOUSE site for important collection against Russian,
Chinese, and **Indian satellite telemetry**.

---

### US aims for "sustained access to Indian leadership communications"

#### 2006-04-07

**Sources:**
  - [Interns Now Contributing from Day One](sources/sidtoday/2006-04-07_SIDToday_-_Interns_Now_Contributing_from_Day_One.pdf)

> (S//SI) Since that time, I have adjusted to the pace of life here at the NSA. I am currently
working in the India Branch of Regional Targets where we are looking for **sustained accesses to
Indian leadership communications** (in a country where face-to-face is the norm).

---

### NZ provides intel to US on Indian diplomatic communications

#### 2013-04-??

**Sources:**
  - [NSA Intelligence Relationship with New Zealand, released by *The New Zealand Herald*](sources/nz-herald/nzodnipaperapr13-v1-0-pdf-redacted.pdf)

> (U)  Subject:  NSA Intelligence Relationship with New Zealand
>
> (U) What Partner Provides to NSA
>
> (TS//SI//REL) GCSB provides collection on China, Japanese/North Korean/Vietnamese/South American diplomatic communications, South Pacific Island nations, Pakistan, India, Iran, and Antarctica; as well as, French police and nuclear testing activities in New Caledonia.

(GCSB = NZ's Government Communications Security Bureau)

Note that GCSB is a partner in SSPAC (SIGINT Seniors Pacific) alongside India's RAW, see [*Intelligence relationship between India and US, 2009*](#intelligence-relationship-between-india-and-us).

---

### US probably launching recon aircraft from India, Thailand, Pak

#### 2001-01-??

**Sources:**
  - [EP-3E Collision: Cryptologic Damage Assessment and Incident Review](sources/snowden-archive/20170410-theintercept__10th_anniversary_edition_ep_3_damage_assessment.pdf)

> (U//FOUO) Areas of Potential Compromise and Recommended Actions
>
> (S//SI) **Fact of staging of US Navy EP-3 missions from Thailand and associated targets
(India/ Pakistan)**
>
> The Office of Foreign Relations recommends **no action be initiated with Indian or Pakistani partner governments**
regarding the existence or origination of these flights or of the targeting of their
communications.

---

### US recon aircraft targeting Indian communications

#### 2001-01-??

**Sources:**
  - [EP-3E Collision: Cryptologic Damage Assessment and Incident Review](sources/snowden-archive/20170410-theintercept__10th_anniversary_edition_ep_3_damage_assessment.pdf)

> (U//FOUO) Areas of Potential Compromise and Recommended Actions
>
> (C) **Fact of US SIGINT System and/or EP-3 targeting of India, Pakistan, Malaysia,
Philippines, and Sri Lanka**
>
> The Office of Foreign Relations recommends **no action be initiated with
these foreign governments**, only two of which are SIGINT partners.

---

### US recon aircraft crash in China made India vulnerable to cyber warfare

#### 2001-01-??

**Sources:**
  - [EP-3E Collision: Cryptologic Damage Assessment and Incident Review](sources/snowden-archive/20170410-theintercept__10th_anniversary_edition_ep_3_damage_assessment.pdf)

> (U) ELINT Damage Assessment
>
> **Exploitation of the EPL and CTEGM could facilitate
PRC electronic warfare planning against the U.S., India, and Taiwan**, and allow the
employment of denial and deception techniques tailored to U.S. knowledge gaps.
>
> The ELINT Evaluator's laptop was left onboard and is considered
compromised. **The laptop contained a comprehensive (worldwide) Electronic Order of
Battle (EOB). Information included locations and names of fixed radar sites, along with
designations of radar systems installed at these sites.**
>
> The EPL reveals the sum of U.S. knowledge at the Secret-level about the
parameters and **operating characteristics of most known radars from both U.S. and
foreign manufacturers.** Signals described range from air traffic control and early warning
radars to airborne intercept radars and **cruise missile seekers.**

(EPL = ELINT Parameter Limits, CTEGM = Collector Technical ELINT
Guidance Manual)

---

### Intelligence relationship between India and US

#### 2009-06-15

NSA details its changing counter-terrorism relationship with RAW.

 - Relationship started in 2002, after 9/11, when India approached NSA about a CT partnership.
 - India became a SIGINT Seniors Pacific (SSPAC) member in 2008.
 - RAW became the largest contributor of SSPAC reports next to the US, and the info shared gathered positive feedback.
 - Since May 2008, RAW and NSA communicated on a spate of terrorist attacks in India.
 - NSA gave timely warning to India about an attack on Indian embassy in Kabul, Afghanistan.
 - Some info leaks on RAW's end, later fixed.
 - > "By the time terrorists struck Mumbai, NSA's relationship with the INSA and RAW had
matured to the point that NSA was willing to share specific TOP SECRET information with India. CT
sharing after Mumbai included phone and fax numbers, e-mail addresses, voice and language
identifications, voice cuts, lead information and interrogation reports."


**Sources:**
  - [NSA’s Changing Counterterrorism Relationship With India](sources/edwardsnowden-com/SIDtoday-2009-06-15-NSAs-Changing.pdf)

---
---

## Sources

Documents in `sources/snowden-archive` are from [iamcryptoki/snowden-archive](https://github.com/iamcryptoki/snowden-archive), which are in turn sourced from various news media outlets, such as *The Intercept* or *Der Spiegel*.

Documents in `sources/sidtoday` are from [firstlookmedia/sidtoday](https://github.com/firstlookmedia/sidtoday), which has the document archive of SIDtoday direct from *The Intercept*.

Documents in `sources/edwardsnowden-com` are from [edwardsnowden.com](https://search.edwardsnowden.com), which are in turn sourced from various news media outlets, such as *The Intercept* or *Der Spiegel*.

Other documents in `sources/` are obtained direct from news media outlets.

---
---

![Public Domain](cc0.png)

By /u/UniversallyUniqueID. Released into the public domain (CC0).

Content in `sources/` belongs to their respective authors.